package net.qqxh.service;

import net.qqxh.persistent.FileNode;
import net.qqxh.persistent.Jfile;
import net.qqxh.persistent.SearchLib;
import net.qqxh.service.common.BaseCallBack;
import net.qqxh.service.task.FileResolveTaskCallBack;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public interface FileService {



    List<Map> queryFileFromES(String esIndex, String content, String path, Integer esFrom, Integer esSize);

    void resolveAllFile(SearchLib searchLib, FileResolveTaskCallBack fileResolveTaskCallBack) throws IOException;

    void resolveFileByPath(SearchLib searchLib, String path, FileResolveTaskCallBack fileResolveTaskCallBack) throws IOException;

    String deleteIndex(SearchLib searchLib);


    String flushAllRedis();



    Object readFile2FormatTree(String key);
    boolean findFileByViewPath(SearchLib searchLib, String viewPath);

    void sendToDesktop(SearchLib searchLib, String path, String userid);

    void removeFromDesktop(SearchLib searchLib, String path, String userid);

    List<FileNode> getMyDesktop(String userid);

    void clearDesktop(String userid);

    void resolveFliesList2TreeNode();

    void addNode2cache(FileNode fileNode, SearchLib searchLib);

}
