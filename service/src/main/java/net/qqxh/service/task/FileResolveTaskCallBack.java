package net.qqxh.service.task;

import net.qqxh.service.model.ResolveCallBackMsg;

public interface FileResolveTaskCallBack {
    void doCallBack(ResolveCallBackMsg msg);
}
