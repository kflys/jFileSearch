package net.qqxh.resolve2text.exception;

/**
 * @author jason
 */
public class File2TextResolveException extends RuntimeException{
    public File2TextResolveException(String message) {
        super(message);
    }
}
